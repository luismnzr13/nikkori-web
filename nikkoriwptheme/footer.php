<footer class="footer">
  <div class="container">
    <div class="columns">
      <div class="column has-text-centered">
        <img class="footer-logo" src="/wp-content/uploads/2019/02/nikkori-logo_w.svg" alt=""> <br>
        <p class="footer-text">© 2019 Nikkori. Todos los derechos reservados.</p>
      </div>
      <div class="column has-text-centered">
        <div class="columns">
          <div class="column">
            <a href="/menu" class="footer-text-link">
              Menú
            </a>
            <a href="/sucursales" class="footer-text-link">
              Sucursales
            </a>
          </div>
          <div class="column">
            <a href="/catering" class="footer-text-link">
              Catering
            </a>
            <a href="/negocio" class="footer-text-link">
              Negocio
            </a>
          </div>
        </div>
      </div>
      <div class="column has-text-centered">
        <p class="footer-text">Nuestras redes</p>
        <a href="https://www.instagram.com/nikkori_sushi"><i class="social-icon fab fa-instagram"></i></a>
        <a href="https://www.facebook.com/nikkori.mx"><i class="social-icon fab fa-facebook-f"></i></a>
        <br>
        <br>
        <p class="footer-text">Patrocinado por</p>
        <img class="footer-logoAlt" src="/wp-content/uploads/2019/03/cocacola.png" alt="">
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded', () => {

    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    if ($navbarBurgers.length > 0) {

      $navbarBurgers.forEach( el => {
        el.addEventListener('click', () => {

          const target = el.dataset.target;
          const $target = document.getElementById(target);

          el.classList.toggle('is-active');
          $target.classList.toggle('is-active');

        });
      });
    }
  });
</script>
 <?php wp_footer();?>
</body>
</html>
