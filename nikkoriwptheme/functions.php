<?php

/*
 * Nikkori WordPress Theme
 *
 * Copyright (c) 2019
 */

 remove_filter( 'the_content', 'wpautop' );
 remove_filter( 'the_excerpt', 'wpautop' );
